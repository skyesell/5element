import { h } from 'preact'
import styles from '../ContactInfoItem/ContactInfoItem.scss'

const ContactInfoItem = (props) => {
  return (
    <div class={styles.item}>
      {props.imgUrl && <img class={styles.logo} src={props.imgUrl} alt={props.imgAlt} />}
      <div class={styles.textInfo}>
        <p class={styles.name}>{props.name}</p>
        <a class={styles.link} href={props.href}>
          {props.children}
        </a>
      </div>
    </div>
  )
}
export default ContactInfoItem
