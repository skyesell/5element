import { h } from 'preact'
import logo from '../../../assets/icons/logo.svg'
import menu from '../../../assets/icons/mobMenu.svg'
import styles from './Header.scss'
import { useState } from 'preact/compat'
import MobMenu from '../mobMenu'

const Header = () => {
  const [menuIsOpened, setOpenedMenu] = useState(false)

  const openMenu = () => {
    setOpenedMenu(true)
  }

  const closeMenu = () => {
    setOpenedMenu(false)
  }

  return (
    <div class={styles.navbar}>
      <img class={styles.logo} src={logo} alt='Пятый элемент' />
      <MobMenu opened={menuIsOpened} onClose={closeMenu} />
      <div onClick={openMenu}>
        <img className={styles.btnMenu} src={menu} alt='Пятый элемент' />
      </div>
      <div class={styles.items}>
        <a href='#services'>Услуги</a>
        <a href='#projects'>Проекты</a>
        <a href='#contacts'>Контакты</a>
      </div>
    </div>
  )
}

export default Header
