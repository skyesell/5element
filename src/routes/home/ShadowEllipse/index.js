import { h } from 'preact'
import styles from './ShadowEllipse.scss'

const ShadowEllipse = ({ size, color }) => {
  return (
    <div
      class={styles.ellipse}
      style={{
        height: `${size}px`,
        width: `${size}px`,
        boxShadow: `0px 0px 100px ${size}px ${color}`,
        background: color,
      }}
    />
  )
}

export default ShadowEllipse
