import { h } from 'preact'
import styles from './home.scss'
import Header from './Header'
import ContactInfoItem from './ContactInfoItem'
import phone from '../../assets/icons/phone.svg'
import email from '../../assets/icons/email.svg'
import logoWhite from '../../assets/icons/logo_white.svg'
import Divider from './Divider'
import infoBlock from '../../assets/icons/infoBlock.png'
import ShadowEllipse from './ShadowEllipse'

const Home = () => {
  return (
    <>
      <Header />
      <div class={styles.headerInfoBlock}>
        <img class={styles.infoBlock} src={infoBlock} alt='Информационный блок' />
        <div class={styles.ellipse}>
          <ShadowEllipse size={130} color={'#0A2DDA'} />
        </div>
        <div className={styles.ellipseLight}>
          <ShadowEllipse size={140} color={'#42E8E0'} />
        </div>
        <div class={styles.corporationInfo}>
          <h1 class={styles.headerInfo}>Пятый элемент</h1>
          <h3 class={styles.textInfo}>
            IT-компания, предоставляющая аутсорс-услуги. Разрабатываем программные и технические
            системы, комплексы для бизнеса в сферах производства, ритейла, медицины, финансов,
            транспорта и логистики{' '}
          </h3>
        </div>
      </div>

      <div className={styles.contactInfo}>
        <ContactInfoItem name='Приёмная' href='tel:+7 (800) 555-30-53' imgUrl={phone}>
          +7 (800) 555-30-53
        </ContactInfoItem>
        <ContactInfoItem name='Почта' href='mailto:rcm@reliab.tech' imgUrl={email}>
          rcm@reliab.tech
        </ContactInfoItem>
      </div>

      <div id='services' class={styles.services}>
        <h4 class={styles.headerLightInfo}>Чем мы занимаемся</h4>
        <div className={styles.ellipse}>
          <ShadowEllipse size={160} color={'#0A2DDA'} />
        </div>
        <h2 class={styles.textDarkInfo}>
          Фокусируемся на разработке программных, технических системах и аутсорс-услугах для бизнеса
        </h2>
        <h3 class={styles.textLightInfo}>Проектирование и разработка программных решений</h3>
        <Divider />
        <h3 class={styles.textLightInfo}>Формализация и документирование бизнес-требований</h3>
        <Divider />
        <h3 class={styles.textLightInfo}>Разработка мобильных приложений для ОС Android и iOS</h3>
        <Divider />
        <h3 class={styles.textLightInfo}>Мобильные решения на платформах iOS и Android</h3>
        <Divider />
        <h3 class={styles.textLightInfo}>UI/UX-дизайн</h3>
        <Divider />
        <h3 class={styles.textLightInfo}>ИТ-аутстаффинг</h3>
        <Divider />
        <h3 class={styles.textLightInfo}>ИТ-аутсорсинг</h3>
        <Divider />
        <h3 class={styles.textLightInfo}>Аудит ИТ-инфраструктуры и процессов</h3>
        <Divider />
      </div>

      <div id='projects' class={styles.projects}>
        <h4 class={styles.headerLightInfo}>Наши проекты</h4>
        <h2 class={styles.textDarkInfo}>
          Специалистами компании реализовано более <span style='color: #0A2DDA;'>100 проектов</span>{' '}
          – корпоративные системы, программные решения (в том числе - приложения для мобильных
          устройств), чат-боты, системы технического зрения.
        </h2>
        <div class={styles.projectInfo}>
          <div class={styles.blueBlock}>
            <img class={styles.logoWhite} src={logoWhite} alt={'Логотип'} />
            <h2 class={styles.headerWhite}>Выделенный центр разработки программного обеспечения</h2>
            <h3 class={styles.textLight}>
              Выделенный центр разработки программного обеспечения организован для поддержки
              ИТ-платформы крупного европейского банка.
            </h3>
          </div>
          <div class={styles.whiteBlock}>
            <h3 class={styles.blackInfo}>
              <p>
                Для заказчика была выделена команда IT-специалистов, подобранных с учетом
                индивидуальных потребностей заказчика, целей и задач проектов. Выполнен анализ
                требований и подбор ресурсов, на протяжении проекта проводился контроль качества
                работы.
              </p>

              <p>
                Реализованы проектирование, создание и доработка программных систем и сервисов для
                различных процессов банка (КК, Customer Expirience, HR, BI). Осуществлялась
                поддержка контуров для разработки и тестирования, CI/CD. Также специалистами ВЦР
                выполнена разработка микросервисной платформы.
              </p>
              <p>
                Разработка велась на технологиях Java, Spring Framework, Spring Cloud, Angular,
                React, REST, Oracle PL/SQL, Swift, Kotlin)
              </p>
            </h3>
            <div className={styles.ellipse}>
              <ShadowEllipse size={180} color={'#0A2DDA'} />
            </div>
          </div>
        </div>
      </div>

      <div id='contacts' class={styles.contactInfoBlock}>
        <h2 class={styles.contact}>Контакты</h2>

        <div>
          <div class={styles.contactInfoItems}>
            <ContactInfoItem name='Телефон' href='tel:+7 (800) 555-30-53'>
              +7 (800) 555-30-53
            </ContactInfoItem>

            <ContactInfoItem name='Почта' href='mailto:rcm@reliab.tech'>
              rcm@reliab.tech
            </ContactInfoItem>
          </div>
          <div className={styles.ellipse}>
            <ShadowEllipse size={100} color={'#0ACEDA'} />
          </div>
          <h3 class={styles.contactHeader}>Офис</h3>
          <h2 class={styles.contactInfoItem}>
            308033, Россия, г. Белгород, ул. Королёва, 2а, корп.2, оф. 536
          </h2>
        </div>
      </div>
      <Divider />

      <footer class={styles.footer}>
        <h5 class={styles.fiveElement}>ООО «Пятый элемент»</h5>
        <p>
          <a class={styles.back} href='#app'>
            Вернуться наверх
          </a>
        </p>
      </footer>
    </>
  )
}

export default Home
