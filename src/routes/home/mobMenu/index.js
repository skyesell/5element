import { h } from 'preact'
import styles from './mobMenu.scss'
import { useEffect, useState } from 'preact/compat'
import logo from '../../../assets/icons/logo.svg'
import closeMenu from '../../../assets/icons/closeMenu.svg'
import logoMenu from '../../../assets/icons/logoMenu.svg'
import cn from 'classnames'

const MobMenu = ({ opened, onClose }) => {
  const [rendered, setRendered] = useState(false)

  useEffect(() => {
    setTimeout(() => setRendered(true), 500)
  }, [])

  const handleClose = () => {
    onClose()
  }

  const wrapperStyles = cn(styles.wrapper, {
    [styles.notRendered]: !rendered,
    [styles.closing]: !opened,
  })

  return (
    <div class={wrapperStyles}>
      <div class={styles.head}>
        <img class={styles.logo} src={logo} alt='Пятый элемент' />
        <img class={styles.btnMenu} src={closeMenu} alt='Закрыть меню' onClick={handleClose} />
      </div>
      <img class={styles.logoMenu} src={logoMenu} alt='Логотип' />
      <div class={styles.items}>
        <a onClick={handleClose} href='#services'>
          Услуги
        </a>
        <a onClick={handleClose} href='#projects'>
          Проекты
        </a>
        <a onClick={handleClose} href='#contacts'>
          Контакты
        </a>
      </div>
      <div class={styles.contacts}>
        <div class={styles.item}>
          <h3>Приёмная</h3>
          <a href={'tel:+7 (800) 555-30-53'}>+7 (800) 555-30-53</a>
        </div>
        <div class={styles.item}>
          <h3>Менеджер</h3>
          <a href={'mailto:tn@reliab.tech'}>tn@reliab.tech</a>
        </div>
      </div>
    </div>
  )
}

export default MobMenu
